#####################################
####### YRO PERIOD ESTIMATION #######
#####################################

#On first run only, uncomment (remove "#") from these two lines to install required packages
#install.packages('readr', repos='http://cran.us.r-project.org')
#install.packages('rpart', repos='http://cran.us.r-project.org')

library(readr)
library(rpart)


###############################
####### USER PARAMETERS #######
###############################
start_time = 2500
granulatory = 0.005
spacing = 50
###############################



############################################
####### DO NOT EDIT BELOW THIS LINE! #######
############################################

#set input file with the specified title and setwd
#######

args = commandArgs(trailingOnly=TRUE)
if (length(args)!= 2) {
    stop("Error reading files!", call.=FALSE)
}
input = args[1]
setwd(args[2])

#read csv file and create one vector per run, assuming 1m time intervals
#######

table <- read.csv(input,row.names = NULL)
raw_data <- as.matrix(table)
number_of_runs = length(raw_data[1,]) - 1
for(i in 1:number_of_runs){
    run <- paste("run",i,sep = "")
    
    tmp_run = as.numeric(raw_data[,i+1])
    tmp_run = tmp_run[!(is.na(tmp_run))]
    assign(run,tmp_run)
}

#fit piecewise to raw data
#######

for(i in 1:number_of_runs){
    run <- paste("run",i,sep = "")
    run_piecewise <- paste(run,"piecewise",sep="_")
    
    t = 1:length(get(run))
    df <- data.frame(y = get(run), x = t)
    fit <- rpart(y ~ x, data = df, control=rpart.control(minsplit=spacing, cp=granulatory))
    tmp_piecewise <- predict(fit, list(x = df$x))
    assign(run_piecewise,tmp_piecewise)
}

#look for change points working backawards until time limit
#######

for(i in 1:number_of_runs){
    run <- paste("run",i,sep = "")
    run_piecewise <- paste(run,"piecewise",sep="_")
    run_changepoints <- paste(run,"changepoints",sep = "_")
    run_piecewise <- get(run_piecewise)
    
    #end_time = 5500
    end_time = length(run_piecewise)
    value = run_piecewise[end_time]
    
    tmp_changepoints = c()
    for(s in end_time:start_time){
        if(run_piecewise[s] != value){
            value = run_piecewise[s]
            tmp_changepoints <- c(tmp_changepoints,s)
        }
    }
    assign(run_changepoints,tmp_changepoints)
}

#plot to confirm changepoints identified correctly
#######

directory_name = paste("output",input,sep="/")
directory_name = paste(directory_name,"plots",sep="_")
dir.create(directory_name, showWarnings = FALSE, recursive = FALSE, mode = "0777")
for(i in 1:number_of_runs){
    run <- paste("run",i,sep = "")
    run_piecewise <- paste(run,"piecewise",sep="_")
    run_changepoints <- paste(run,"changepoints",sep = "_")
    run_piecewise <- get(run_piecewise)
    
    filename <- paste(run,"pdf",sep=".")
    filename <- paste(directory_name,filename,sep="/")
    pdf(file = filename)
    t = 1:length(get(run))
    plot(t,get(run),type="l",ylab="DO",xlab="time (mins)")
    lines(t,run_piecewise, col = "red")
    points(t[get(run_changepoints)],run_piecewise[get(run_changepoints)],type="o",col="blue")
    dev.off()
}

#extract period and plots for each run
#######

file_name <- paste("output",input,sep="/")
file_name <- paste(file_name,"results.txt",sep="_")
file.create(file_name)


for(i in 1:number_of_runs){
    run <- paste("run",i,sep = "")
    run_changepoints <- paste(run,"changepoints",sep = "_")
    run_period <- paste(run,"period",sep = "_")
    run_changepoints <- get(run_changepoints)
    
    odd_changepoints <- run_changepoints[seq(1,length(run_changepoints),by=2)]
    even_changepoints <- run_changepoints[seq(2,length(run_changepoints),by=2)]
    
    odd_diffs = rev(-diff(odd_changepoints))
    even_diffs = rev(-diff(even_changepoints))
    
    cycles_odd = 1:length(odd_diffs)
    cycles_even = 1:length(even_diffs)
    
    V_odd = 1/odd_diffs
    S_odd = 1/cycles_odd
    lin_odd <- lm(V_odd ~ S_odd)
    P_start_odd = 1/coef(lin_odd)[1]
    KP_start_odd = P_start_odd*coef(lin_odd)[2]
    fit_odd <- nls(odd_diffs ~ Period_odd*cycles_odd/(cycles_odd + KP_odd), start = list(Period_odd = P_start_odd, KP_odd = KP_start_odd))
    
    V_even = 1/even_diffs
    S_even = 1/cycles_even
    lin_even <- lm(V_even ~ S_even)
    P_start_even = 1/coef(lin_even)[1]
    KP_start_even = P_start_even*coef(lin_even)[2]
    fit_even <- nls(even_diffs ~ Period_even*cycles_even/(cycles_even + KP_even), start = list(Period_even = P_start_even, KP_even = KP_start_even))
    
    coeffs_odd = as.numeric(coef(fit_odd))
    coeffs_even = as.numeric(coef(fit_even))
    mean_coeffs = (coeffs_odd + coeffs_even)/2
    names(mean_coeffs) = c("Period","K_P")
    fit_curve_odd = predict(fit_odd)
    fit_curve_even = predict(fit_even)
    
    write(run,file=file_name,append=TRUE)
    write("\n",file=file_name,append=TRUE)
    result <- paste("Period",mean_coeffs[1],sep=": ")
    result <- paste(result,mean_coeffs[2],sep = " KP: ")
    write(result,file=file_name,append=TRUE)
    write("\n",file=file_name,append=TRUE)
    write.table(odd_diffs,file=file_name,append=TRUE,col.names=FALSE,row.names=FALSE)
    write("\n",file=file_name,append=TRUE)
    write.table(even_diffs,file=file_name,append=TRUE,col.names=FALSE,row.names=FALSE)
    write("\n",file=file_name,append=TRUE)
    
    max_cycles = max(length(odd_diffs),length(even_diffs))
    filename <- paste(run,"pdf",sep="_differences.")
    filename <- paste(directory_name,filename,sep="/")
    pdf(file = filename)
    plot(odd_diffs,xlim = c(0,max_cycles),ylab="differences",xlab="cycle number")
    points(even_diffs, col = "red")
    lines(fit_curve_odd,lty=2,lwd=3)
    lines(fit_curve_even,lty=2,col="red",lwd=3)
    dev.off()
}













