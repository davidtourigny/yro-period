 
 
\documentclass[A4,12pt]{amsart}
\usepackage{amssymb,stmaryrd}
\usepackage{amsfonts}
\usepackage{amstext}
\usepackage{algorithmic}
\usepackage{algorithm}
%\usepackage[arrow,matrix,cmtip]{xy}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage[all]{xy}
\usepackage{MnSymbol}
%\usepackage[latin1]{inputenc}
%\usepackage{theorem}
%\usepackage{thc}
\textwidth 14cm
\parindent 0cm
\parskip 6pt plus 1pt minus1pt
\arraycolsep 1pt
%\global\setlength\theorempreskipamount{8pt plus 4pt minus 1pt}
%\global\setlength\theorempostskipamount{8pt plus 3pt minus 1.5pt}

\numberwithin{equation}{section}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{question}[theorem]{Question}
\newtheorem{fact}[theorem]{Fact}
\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{case}[theorem]{Case}
\newtheorem{conjecture}[theorem]{Conjecture}
\theoremstyle{remark}
\newtheorem{remark}[theorem]{\bf{Remark}}
\newtheorem{note}[theorem]{\bf{Note}}
\newtheorem{counterexample}[theorem]{\bf{Counter-example}}
\newtheorem{problem}[theorem]{\bf{Problem}}
\newtheorem{convention}[theorem]{\bf{Conventions}}
\newtheorem{notation}[theorem]{\bf{Notation}}

%% fonts
\newcommand{\R}{{\mathbb{R}}}
\newcommand{\C}{{\mathbb{C}}}
\newcommand{\Z}{{\mathbb{Z}}}
\newcommand{\Q}{{\mathbb{Q}}}
\newcommand{\N}{{\mathbb{N}}}
\renewcommand{\O}{{\mathbb{O}}}
\newcommand{\CA}{{\mathcal{A}}}
\newcommand{\CC}{{\mathcal{C}}}
\newcommand{\CE}{{\mathcal{E}}}
\newcommand{\CD}{{\mathcal{D}}}
\newcommand{\CF}{{\mathcal{F}}}
\newcommand{\CR}{{\mathcal{R}}}
\newcommand{\CS}{{\mathcal{S}}}
\newcommand{\CL}{{\mathcal{L}}}
\newcommand{\CM}{{\mathcal{M}}}

%% symbols
\newcommand{\Ext}{{\rm{Ext}}}
\newcommand{\Hom}{{\rm{Hom}}}
\newcommand{\Sym}{{\rm{Sym}}}
\newcommand{\Aut}{{\rm{Aut}}}
\newcommand{\<}{{\langle}}
\renewcommand{\>}{{\rangle}}
\newcommand{\Tr}{{\rm Trace}}
\newcommand{\la}{{\triangleright}}
\newcommand{\ra}{{\triangleleft}}
\renewcommand{\ker}{{\rm{Ker}}}
\newcommand{\im}{{\rm{Im}}}
\newcommand{\tens}{\otimes}
\newcommand{\id}{{\rm id}}
\newcommand{\extd}{{\rm d}}
\newcommand{\del}{{\partial}}
\newcommand{\fdel}{{\delta}}
\newcommand{\eps}{\epsilon}
\newcommand{\ev}{{\rm ev}}
\newcommand{\coev}{{\rm coev}}
\newcommand{\und}{\underline}

% hopf algebra suffices

\newcommand{\bo}{{}_{\bar{(1)}}}
\newcommand{\bz}{{}_{\bar{(0)}}}
\newcommand{\lz}{{}_{\bar{(\infty)}}}
\renewcommand{\o}{{}_{(1)}}
\renewcommand{\t}{{}_{(2)}}
\renewcommand{\th}{{}_{(3)}}



% following probably not going to be used

\def\rcross{{\triangleright\!\!\!<}}
\def\lcross{{>\!\!\!\triangleleft}}
\def\rcocross{{\blacktriangleright\!\!<}}
\def\lcocross{{>\!\!\blacktriangleleft}}
\def\cobicross{{\triangleright\!\!\!\blacktriangleleft}}
\def\bicross{{\blacktriangleright\!\!\!\triangleleft}}
\def\dcross{{\bowtie}}
\def\codcross{{\blacktriangleright\!\!\blacktriangleleft}}

\begin{document}



\title{{\em YROPERIOD} documentation}
%\keywords{}

%\subjclass[2000]{Primary 81R50, 58B32, 83C57}
%\thanks{}

\author{David S. Tourigny}
\address{Columbia University Medical Center, 630 W 168th St, New York, NY 10032}

\email{dst2156@cumc.columbia.edu}

%\thanks{}
%\date{}


\begin{abstract}    
This document describes the package {\em YROPERIOD} that performs period estimation for experimental data from the yeast respiratory oscillation (YRO). Section \ref{sec:problem} gives a general overview of the problem and the method of solution implemented in {\em YROPERIOD}. Section \ref{sec:info} provides instructions for running the software.    
\end{abstract}
\maketitle 


\section{Problem statement and methodology}
\label{sec:problem}
Periodicity is a well-defined concept in mathematics. A function $f$ of some variable, e.g. time ($t$), is said to be periodic with period $T$ if, for some real number $T>0$, the function satisfies $f(t+T) = f(t)$ for all $t>0$. The examples perhaps most familiar to the average user of {\em YROPERIOD} are trigonometric functions $\cos$ and $\sin$, which are both periodic with period $2 \pi$ (e.g. $\cos(t + 2 \pi) = \cos(t)$). Often in biology one encounters time course data (i.e., a sequence of experimental measurements made at ordered time points) that appear periodic. A clear distinction between these data and the above definition of periodicity should be made however, for two important reasons. First, the above definition relies on the fact that the independent variable (in this case time) is continuous. Time course data can be assumed to originate from an underlying continuous process, but the actual measurements occur at discrete time points $t_0, t_1, t_2, ...$. Although periodicity can equivalently be defined for functions taking on values over a discrete domain, it is very unlikely that a time course will be truly periodic by this definition either (e.g. think about taking discrete measurements of $\cos(t)$ at time points $t_0, t_1, t_2, ...$, this time course will only be truly periodic if $t_{i + N} = t_i + 2 \pi$ for some natural number $N$). Secondly, biological processes and experimental measurements are often distorted by noise or drifts in time, and therefore this will also break down any notion of true periodicity. In short, the only thing one should really say about time course data is that they are approximately periodic.

This does not imply it is useless to discuss periodicity in the context of time series however! For many time course data, such as the dissolved oxygen (DO) traces of the YRO, the approximation of periodicity is a very good one. This means one can effectively discuss notions of a period given the understanding that it is defined in relation to the truly periodic function that the YRO is approximating. How does one estimate this period from the experimental data? There are several ways to estimate period from time course data and each method has its strengths and limitations. One common method is to use Fourier analysis, which in many cases gives a very good estimate of period, but struggles to do so when there are relatively few periods of the oscillation (period estimation using Fourier analysis approaches perfection as the time series grows infinitely long). It also does not cope very well when the apparent ``period'' drifts over time (in this case not even the function the data are approximating is truly periodic, something that also occurs many times in biology), but more advanced tools such as wavelet analysis have been developed to accommodate this property. Perhaps the most naive approach to approximating period is to identify ``equivalent'' reference points on the time course and directly measure the horizontal difference between them. One could devise many ways to measure these differences, and combine or average them to get a rough approximation of period. It would however, in general, be very hard to automate this process because you would need to tell your machine exactly which points to use and what to do with them. The second ``what to do" part is not so difficult, but the first ``which points" problem is deceptively complicated. I will not spell out reasons for why this is very often likely to fail, except to direct the user towards thinking about how to implement automated period estimation for the $\cos(t)$ time course above (which points on the time course to choose, and how to choose these without knowing the period beforehand?). Here a simple procedure to be carried out by hand becomes very complicated to translate into an algorithm. However, automation is imperative for eliminating user bias and employing a consistent definition of period when comparing results across multiple experiments.   

In spite of the difficulties associated with this approach, {\em YROPERIOD} does in fact implement a version of the second method described above for automated period estimation. It does so by exploiting a particular property of YRO time course data-- that they can be well-approximately by a piece-wise constant function. The algorithm implemented by {\em YROPERIOD} consists of three phases: 1) a regression (fit) of the YRO time course data using a piece-wise constant model (based on regression trees, beyond the scope of this documentation); 2) identifying equivalent points on the resulting piece-wise constant model (as peaks and troughs) and calculating the horizontal differences between them; and 3) nonlinear regression of a Michaelis-Menten model to these time-ordered horizontal differences and extracting the saturation constant to give an estimation of period. The Michaelis-Menten model is defined by the equation
\begin{equation*}
\mbox{Period} \times \frac{\mbox{cycle number}}{K_P + \mbox{cycle number}}
\end{equation*}
where cycle number indexes the successive pairs of peaks or troughs used to calculate horizontal differences. Phase 3 is based on the empirical observation that the differences calculated in Phase 2 tend to increase before saturating to some approximately constant value over the course of the YRO. As a consequence of Phase 3, {\em YROPERIOD} also estimates the constant $K_P$ as an analogue of the familiar Michaelis constant $K_M$, which effectively measures the rate of saturation. Returning to the underlying function that YRO time course data appear to be approximating, these observations suggest it is in fact a limit cycle oscillation (not a truly periodic function), which is consistent with our mathematical understanding of the YRO. This means that the method employed by {\em YROPERIOD} has greater power than nonlinear regression with a truly periodic function (such as a sinusoidal curve), which would always predict a constant period. For each time course, {\em YROPERIOD} provides an estimate of the period of this limit cycle and the relative rate of convergence to the limit cycle from the given experimental initial conditions.                                             

\section{Instructions for running yroperiod}
\label{sec:info}
\subsection{Installation}
{\em YROPERIOD} is based on the {\em R} programming language and so does not need installation per se, but does therefore require that you have {\em R} installed on your computer (available for free at {\em https://www.r-project.org/}). In addition, {\em YROPERIOD} uses two {\em R} packages that do not come pre-installed with {\em R}. These are {\em readr} and {\em rpart}. Before running {\em YROPERIOD} for the first time you must make sure these packages are installed on your computer. The simplest way of doing so is to open a new instance of {\em R} in the {\em Terminal} window (you can do this by entering lowercase {\em r} into the command line). Then, to install the packages enter the command {\em install.packages(`readr', repos=`http://cran.us.r-project.org')} into the {\em R} command line, wait for the installation of {\em readr} to finish, and then repeat with {\em install.packages(`rpart', repos=`http://cran.us.r-project.org')} to install {\em rpart}. Once both packages are installed you can close the instance of {\em R} by entering {\em q()} into the {\em R} command line and exiting without saving the workspace image. The packages should be installed on your computer and {\em YROPERIOD} will be able to access them without problems.
\subsection{Files and input format}
You will see that this repository contains several files. Three files are example input files in the {\em .csv} format required for running {\em YROPERIOD}. Opening one of these will reveal the input structure (Figure \ref{fig:fig1}).  
\begin{figure}
\centering
\includegraphics[width=12cm]{fig1}
\caption{Example input file structure.}
\label{fig:fig1}
\end{figure}
The first column of the input file is always ignored by {\em YROPERIOD} and can in theory contain anything, but here contains a copy of the ``Duration" columns from one of the {\em Excel} ``Data" tabs containing raw bioractor data. The remaining (in this case four) columns contain the raw DO traces from each ``Data" tab of an {\em Excel} file (title of each column, i.e. row 1, is also ignored and can be used for user reference). Each column corresponds to a DO trace from a single vessel and initiates one round of period estimation. Any number can be used in the input file (including combining vessels from multiple experiments), but bear in mind that these will all be processed using the same {\em YROPERIOD} parameters and the results will all go to the same output file (see Results and outputs). It is up to the user to choose how they arrange and construct input files beyond the required format-- perhaps the simplest way of doing so is to copy each DO trace to an empty {\em Excel} file (starting at column 2) and then save the file to {\em .csv} format using {\em Excel}. In many cases it is most convenient to have a separate input file for each experiment (e.g. with data from 1-4 vessels). Multiple files corresponding to multiple experiments can be processed simultaneously in a single execution of {\em YROPERIOD} (see Running {\em YROPERIOD}) and each experiment will have its own output file (see Results and outputs). 

The file {\em yro\_period.r} contains the  {\em YROPERIOD} period estimation script (Figure \ref{fig:fig2}). The user will only need to access this file if they want to modify the parameters {\em start\_time}, {\em granularity}, and {\em spacing} on lines 16-18 (discussed in User parameters and interpretation). The user is warned not to edit anything below line 19 because this could interfere with the performance of {\em YROPERIOD}.
 
\begin{figure}
\centering
\includegraphics[width=14cm]{fig2}
\caption{First lines of the {\em YROPERIOD} script {\em yro\_period.r}.}
\label{fig:fig2}
\end{figure}

The final file {\em yro\_period.sh} contains the shell script that is used for running {\em YROPERIOD}.

\subsection{Running {\em YROPERIOD}} 
Once new input file(s) have been created by the user they must be copied into the directory {\em yroperiod} (just like example files {\em B156.csv}, {\em B160.csv}, and {\em B161.csv}). The folder can contain any number of input files (but at least one!) and any subset of these to be used during a particular execution of {\em YROPERIOD} are specified by the user. To run {\em YROPERIOD} the user must set {yroperiod} as their current working directory in a {\em Terminal} window and call the {\em yro\_period.sh} script from there. This is done by entering the command {\em ./yro\_period.sh FILES} into the {\em Terminal} command line where {\em FILES} is replaced by the list of input files the user wants to process separated by spaces. For example, should the user want to process the example input file {\em B160.csv} they would do this using the command {\em ./yro\_period.sh B160.csv}, and if the user wants to process all three example files simultaneously they could do this using the command {\em ./yro\_period.sh B156.csv B160.csv B161.csv} (order not important; Figure \ref{fig:fig3} for explicit demonstration). Likewise for any pair of input files. There is no limit on the number of input files to be processed simultaneously, but remember all named files must be present in the folder {\em yroperiod}.  

\begin{figure}
\centering
\includegraphics[width=14cm]{fig3}
\caption{Explicit demonstration of command for running {\em YROPERIOD} with all three example input files simultaneously}
\label{fig:fig3}
\end{figure}    
After entering the above execution command, {\em YROPERIOD} will process each input file sequentially (Note: you may get warning/error messages or other information from {\em R} depending on the working environment your version is using. Please send me a copy of these so that I can make {\em YROPERIOD} as clean as possible). 

\subsection{Results and output files}
The first time running {\em YROPERIOD} will create a new folder in {\em yroperiod} called {\em output}. All subsequent runs will output data to this folder and overwrite any files with the same name, and so it is important to transfer results to a new folder should you want to keep them. There are two outputs corresponding to each input file. These are labelled according to the name of the input file, e.g. the folder {\em B160.csv\_plots} and the file {\em B160.csv\_results.txt} correspond to the example input file {\em B160.csv}. The {\em results.txt} output file (Figure \ref{fig:fig4}) contains the estimated Period (in mins) and $K_P$ for each column in the input file (to avoid confusion, {\em YROPERIOD} uses the terminology ``run" when referring to an instance of period estimation for each column of the input file. If there are four columns corresponding to vessels 1-4 from a single experiment, then run number means vessel number, but the user is also free to leave out vessels or include data from multiple experiments in the input file). It also contains the sequence of peak/trough differences for each run in case the user would like to perform period estimation from these data using a more sophisticated regression model than the Michaelis-Menten equation. Results from each run are separated and annotated by run number.   

\begin{figure}
\centering
\includegraphics[width=10cm]{fig4}
\caption{First lines of the output file {\em B160.csv\_results.txt}.}
\label{fig:fig4}
\end{figure}    

The folder {\em plots} contains two {\em .pdf} files corresponding to each run of a {\em YROPERIOD} input file. The first contains a plot of the YRO time course used for that run (in black), and the fit of the piece-wise constant model (in red). In blue circles are the peaks and troughs that {\em YROPERIOD} identified for that run with connecting blue lines to guide the eye. For example, the file {\em run1.pdf} corresponding to the results of Figure \ref{fig:fig4} is displayed in Figure \ref{fig:fig5}. The main purpose of this file is for the user to confirm that {\em YROPERIOD} is identifying peaks and troughs correctly during Phases 1 and 2. It has done so correctly in this example, as the user can verify by observing that every second blue circle always maps to an equivalent point on the piece-wise constant curve and thus, indirectly, the YRO time series. Starting from the far right-hand side of the plot one finds that the first blue circle denotes a peak and the second a trough. Subsequently, moving to the left, every odd (even) blue circle maps to the equivalent peak (trough) until the last is encountered. The point at which peaks and troughs stop being counted and recorded by blue circles is defined by the user (user parameter {\em start\_time}, see User parameters and interpretation). Comparing the left-hand portion of the plot, which includes initialisation of the oscillation, one can see that the choice of this parameter is important because the piece-wise constant regression performs very differently in this region of the time series and so these data should not be included in period estimation. 

\begin{figure}
\centering
\includegraphics[width=12cm]{fig5.pdf}
\caption{File {\em run1.pdf} from the example in Figure \ref{fig:fig4}.}
\label{fig:fig5}
\end{figure}  

Figure \ref{fig:fig6} displays an example where Phases 1 and 2 of {\em YROPERIOD} have been unsuccessful. It corresponds to run 3 of the example in Figure \ref{fig:fig4}. In this case the piece-wise regression in Phase 1 has resulted in too many inflection points, which means some of the blue circles do not label the peaks and troughs that a user would identify as the correct ones by eye. Although by trial-and-error a persistent user may identify values for the parameters {\em granularity} and {\em spacing} that result in Phases 1 and 2 performing correctly (see User parameters and interpretation), often it is the quality of the YRO time series that means {\em YROPERIOD} fails at this stage. Indeed, by comparing the YRO time course in {\em run3.pdf} with the other three {\em run.pdf} files in this example it becomes clear that it does not represent a conventional run (e.g. stops oscillating near end of experiment). As a general rule, runs that do not perform according to the standard of the majority should be excluded as outliers and not taken into consideration for further analysis.                    

\begin{figure}
\centering
\includegraphics[width=12cm]{fig6.pdf}
\caption{File {\em run3.pdf} from the example in Figure \ref{fig:fig4}.}
\label{fig:fig6}
\end{figure}   

Once the user has confirmed the performance of Phases 1 and 2 it is recommended that they then check the performance of Phase 3. This can be done by inspecting the corresponding {\em run\_differences.pdf} files, which plot the differences between successive peaks and troughs in black and red circles for each run (no consistent colouring scheme as this depends on whether a peak or trough was encountered first). The Michaelis-Menten regression models are also overlaid in dashed lines of the same colour. There are two regression model curves, one for each set of differences, and the values Period and $K_P$ in the results file {\em results.txt} are averages of the parameters estimated from both. An example of this is contained in Figure \ref{fig:fig7}, which displays the file {\em run1\_differences.pdf} that corresponds to {\em run1.pdf} (Figure \ref{fig:fig5}), the previous example of a successful run. The user is encouraged to check that the Michaelis-Menten regression model provides a good description of the differences, that these have suitably saturated, and that both peak and trough curves are relatively similar. This is usually the case if Phases 1 and 2 performed well and the experiment was left long enough, but to illustrate a problematic run Figure \ref{fig:fig8} displays the example {\em run3\_differences.pdf} that corresponds to {\em run3.pdf} (Figure \ref{fig:fig6}) above.   

\begin{figure}
\centering
\includegraphics[width=12cm]{fig7.pdf}
\caption{File {\em run1\_differences.pdf} corresponding to {\em run1.pdf} in Figure \ref{fig:fig5}.}
\label{fig:fig7}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=12cm]{fig8.pdf}
\caption{File {\em run3\_differences.pdf} corresponding to {\em run3.pdf} in Figure  \ref{fig:fig5}.}
\label{fig:fig8}
\end{figure}

Provided the user is confident that all three phases of {\em YROPERIOD} have performed satisfactorily for a given run the corresponding estimates of Period and $K_P$ can be used directly, or the peak and trough differences can be exported for further analysis.

\subsection{User parameters and interpretation}
The previous section described the way a user can check that the Period and $K_P$ estimates provided by {\em YROPERIOD} are reliable for a given run. Outliers (i.e., one or more runs performing poorly whilst the majority perform well) are most likely to arise because the DO time courses corresponding to those runs are not of high enough quality and the results should therefore be discarded. However, if most or all runs do not perform well and the user is confident that data are of good quality it may be that parameter values need to be redefined for {\em YROPERIOD}. There are three parameters immediately available to be redefined by the user, and these are specified on lines 16-18 of {\em yro\_period.r} (see Figure \ref{fig:fig2}). The user is free to change these parameter values by modifying and then saving (under same name and location) the script directly, but care should be taken when doing so.

As described in the previous section, the parameter {\em start\_time} specifies when to start counting peaks and troughs (labeled by blue circles). This parameter should be chosen so that the first (counting from left) peak or trough is correctly identified for each run. It effectively tells {\em YROPERIOD} the time at which the reliable part of the oscillation begins, but the user should be aware that the same value of {\em start\_time} will be used for all runs and so should not be smaller than the largest of all reliable values (for all runs to be used for analysis).   

The parameters {\em granularity} and {\em spacing} should be modified by trial-by-error on a more sparing basis. They come in useful when the user feels that {\em YROPERIOD} is not performing well on Phases 1 and 2. The default value of {\em granularity} is 0.005 and that of spacing {\em spacing} is 50. These parameter values worked very well for runs 1, 2, and 4 in the input file example {\em B160.csv} above, but {\em YROPERIOD} failed to perform well with these values during run 3. The interested user might like to experiment with these parameters by seeing what happens when {\em YROPERIOD} is executed on {\em B160.csv} with {\em granularity} set to 0.010. For this value of {\em granularity} run 3 performs much better, and by inspecting the resulting {\em run3.pdf} plot one finds that peaks and troughs are labelled correctly. This comes at a cost to the other runs however, where upon further inspection the user will observe that for these runs many peaks and troughs (particularly in run 4) have been missed altogether even though the value of {\em start\_time} remained the same. This example highlights a failure at Phase 1, the piece-wise constant regression phase, resulting in a decrease in overall reliability of runs 1, 2, and 4. The lesson learnt is that nothing can compensate for data quality, and since the same definition of Period (i.e., the same parameter values) must be used to reliably compare experiments, it is much more sensible to exclude bad data altogether. Another example of data inappropriate for processing with {\em YROPERIOD} can be found in {\em B156.csv}, where a change in temperature over the course of the experiment has resulted in a change in the DO trace baseline. In theory, a knowledgeable user can further modify the {\em yro\_period.r} script to accommodate these data, but one does so at their own peril!               
 
\end{document}

